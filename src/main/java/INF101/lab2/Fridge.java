package INF101.lab2;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class Fridge implements IFridge{

    ArrayList<FridgeItem> fridge = new ArrayList<FridgeItem>();
    
    @Override
    public int nItemsInFridge(){
        int count = 0;
        for (FridgeItem elem : fridge){
            count += 1;
        }
        return count;
    }

    int max_size = 20;
    @Override
    public int totalSize(){
        return max_size;
    }

    @Override
	public boolean placeIn(FridgeItem item){
        if (nItemsInFridge() < max_size){
            fridge.add(item);
            return true;
        }
        else{
            return false;
        }
    }

    @Override
	public void takeOut(FridgeItem item){
        if (fridge.contains(item)){
            fridge.remove(item);
        }
        else{
            throw new NoSuchElementException();
        }
    }

    @Override
	public void emptyFridge(){
        fridge.clear();
    }

    ArrayList<FridgeItem> expired = new ArrayList<FridgeItem>();
    ArrayList<FridgeItem> notExpired = new ArrayList<FridgeItem>();

    @Override
	public List<FridgeItem> removeExpiredFood(){
        for (FridgeItem elem : fridge){
            if (elem.hasExpired() == true){
                expired.add(elem);
            }
            else{
                notExpired.add(elem);
            }  
        }
        fridge.clear();
        for (FridgeItem elem : notExpired){
            fridge.add(elem);
        }
        return expired;
    }
}